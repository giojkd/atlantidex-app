import React, {useState} from "react";
import {StyleSheet, Text, View, TouchableOpacity, TouchableHighlight, TouchableNativeFeedback, Button} from "react-native";

const FlexBox = ({navigation, route}) => {
    const [flexDireaction,setFlexDirection] = useState('column');
    const directions = ['column','row'];
    
    return (
        <>
        <View
            style={[
                styles.container,{
                    flexDirection: flexDireaction
                }
            ]}
        >
            <View style={{flex:2, backgroundColor: "green"}}/>
            <View style={{flex:4, flexDirection:flexDireaction}}>
                <View style={{flex:2, backgroundColor: "white"}}/>
                <View style={{flex:2, backgroundColor: "red"}}/>
            </View>
        </View>
        <View style={{flex:2}}>
            {directions.map((direction,key) => (
                <TouchableOpacity
                    style={[
                        styles.button,
                        direction == flexDireaction && styles.selectedButton
                    ]}
                    key={key}
                    onPress={() => setFlexDirection(direction)}
                >
                    <Text>{direction}</Text>
                </TouchableOpacity>
            ))}
        </View>
      </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 4
    },
    button:{
        padding: 20
    },
    selectedButton:{
        backgroundColor:'yellow'
    }
});

export default FlexBox;