import React, {useContext, useState,useRef} from "react";
import { Text, View, Button } from "react-native";
import { SessionContext } from '../contexts/SessionContext';
import { retriveData, storeData, apiClient } from '../tools/Utils';


export default QrCode = ({ navigation, route }) => {
  let session = useContext(SessionContext);
  async function onDisplayNotification() {
    /*
    // Create a channel
    const channelId = await notifee.createChannel({
      id: "default",
      name: "Default Channel",
    });

    // Display a notification
    await notifee.displayNotification({
      title: "Notification Title",
      body: "Main body content of the notification",
      android: {
        channelId,
        smallIcon: "name-of-a-small-icon", // optional, defaults to 'ic_launcher'.
      },
    });
    */
  }

  React.useEffect(() => {
   session.setRole('Admin');
  },[]);

  return (
    <View>
      <Button
        title="Display Notification"
        onPress={() => onDisplayNotification()}
      />
    </View>
  );

};
