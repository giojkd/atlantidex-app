import { SafeAreaView, Text, View, ActivityIndicator, Button } from 'react-native';
import React,{useContext, useState,useRef} from 'react';
import { SessionContext } from '../contexts/SessionContext';
import {WebView} from 'react-native-webview';
import { retriveData, storeData, apiClient } from '../tools/Utils';
import {BACKEND} from "@env";


export default HomeScreen = ({navigation, route}) => {

  let session = useContext(SessionContext);
  const [isLoading, setLoading] = useState(false);
  const ref = useRef(null);
  React.useEffect(() => {
    if(route.params?.headerLeftInfo) {
      navigation.setOptions({
        headerLeft:(props) => {
          return (
            <Button 
              title={route.params.headerLeftInfo.title}
              color="#fff"
              onPress={route.params.headerLeftInfo.onPress}
            />
          )
        } 
      });
    } else {
      navigation.setOptions({
        headerLeft:(props) => {
          return null;
        } 
      });  
    }
  }, [route.params?.headerLeftInfo]);

  /*React.useEffect(() => {
    retriveData('apitoken').then((value) => tokenLogin(value));
  }, []);*/

  React.useEffect(() => {
    retriveData('pushtoken').then((value) => storePushtoUser(value));
  }, [session.userid]);

  const storePushtoUser = async (token) => {
    if (session.userid) {
      await apiClient.post('api/pushtoken', { 
        pushtoken:token,
        userid:session.userid,
      })
      .then(r => {
        //console.log(r.data);
      })
    }
  }

  /*const tokenLogin = async (apitoken) => {
    session.setApitoken(apitoken);
  }*/

  const runFirst = `
      const allParas = document.getElementsByTagName("header");
      allParas[0].classList.add("d-none");
      true; // note: this is required, or you'll sometimes get silent failures
  `;

  const runBeforeFirst = `
      window.isNativeApp = true;
      true; // note: this is required, or you'll sometimes get silent failures
  `;

  handleWebViewNavigationStateChange = (navState) => {

    console.log('navigation log start');
    console.log(navState);
    if (navState.canGoBack && navState.url !== 'http://atlantidex.eu.ngrok.io/it-it' ) {
      navigation.setParams({
        headerLeftInfo: {
          title: "indietro",
          onPress: () => ref.current.goBack(),
        },
      });
    } else {
      navigation.setParams({
        headerLeftInfo: null,
      });
    }
  };

  function onMessage(data) {
    const myArray = data.nativeEvent.data.split(",");
    session.setUserid(myArray[0]);
    session.setRole(myArray[2]);
    if (!session.apitoken) {
      session.setApitoken(myArray[1]);
      storeData('apitoken',myArray[1]);
    }
  }

  return (
    <SafeAreaView
      style={{
        flex: 1,
      }}
    >
      <WebView
        ref={ref}
        originWhitelist={["*"]}
        source={{ uri: BACKEND + (route.params?.page ? route.params.page : '')  + (session.apitoken ? "?token="+session.apitoken : "") }}
        onError={(syntheticEvent) => {
          const { nativeEvent } = syntheticEvent;
          console.warn("WebView error: ", nativeEvent);
        }}
        onLoad={(syntheticEvent) => {
          const { nativeEvent } = syntheticEvent;
        }}
        onLoadStart={(syntheticEvent) => {
          setLoading(true);
        }}
        onLoadEnd={(syntheticEvent) => {
          setLoading(false);
        }}
        onMessage={onMessage}
        pullToRefreshEnabled={true}
        applicationNameForUserAgent="AtlantidexApp"
        injectedJavaScript={runFirst}
        injectedJavaScriptBeforeContentLoaded={runBeforeFirst}
        onNavigationStateChange={handleWebViewNavigationStateChange}
        allowsBackForwardNavigationGestures
      />
      {isLoading && (
          <View style={{ flex:1, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(52, 52, 52, 0)' }}>
            <ActivityIndicator size="large" />
          </View>
        )}
    </SafeAreaView>
  );
};