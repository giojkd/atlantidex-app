import { SafeAreaView, Text, View, ActivityIndicator, Button, Image, TouchableWithoutFeedback } from 'react-native';
import React,{useState,useRef} from 'react';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { retriveData, storeData, apiClient } from '../tools/Utils';
import {BACKEND} from "@env";


export default PageScreen = ({navigation, route}) => {
  const [isLoading, setLoading] = useState(false);
  const ref = useRef(null);
  React.useEffect(() => {
    if(route.params?.headerLeftInfo) {
        navigation.setOptions({
            headerLeft:(props) => {
              return (
                <Button 
                  title={route.params.headerLeftInfo.title}
                  color="#000"
                  onPress={route.params.headerLeftInfo.onPress}
                 />
              )
            }
        });
    } else {
        navigation.setOptions({
            headerLeft:(props) => {
              return (
                <TouchableWithoutFeedback onPress={() => navigation.toggleDrawer()}>
                  <Image
                      source={require('../assets/toggle-drawer-icon@3x.ios.png')}
                      fadeDuration={0}
                      style={{margin:10}}
                      
                  />
                </TouchableWithoutFeedback>
              )
            }
        });
    }
  }, [route.params?.headerLeftInfo]);

  const runFirst = `
      const allParas = document.getElementsByTagName("header");
      allParas[0].classList.add("d-none");
      true; // note: this is required, or you'll sometimes get silent failures
  `;

  const runBeforeFirst = `
      window.isNativeApp = true;
      true; // note: this is required, or you'll sometimes get silent failures
  `;
  
  

  handleWebViewNavigationStateChange = (navState) => {

    console.log('navigation log start');
    console.log(navState);
    if (navState.canGoBack) {
      navigation.setParams({
        headerLeftInfo: {
          title: "indietro",
          onPress: () => ref.current.goBack(),
        },
      });

    } else {
      navigation.setParams({
        headerLeftInfo: null,
      });
    }
  };

  

    

  return (
    <SafeAreaView
      style={{
        flex: 1,
      }}
    >


      <WebView
        ref={ref}
        originWhitelist={["*"]}
        source={{ uri: BACKEND+route.params?.page }}
        onError={(syntheticEvent) => {
          const { nativeEvent } = syntheticEvent;
          console.warn("WebView error: ", nativeEvent);
        }}
        onLoad={(syntheticEvent) => {
          const { nativeEvent } = syntheticEvent;
          //console.warn("WebView loaded: ", nativeEvent);
        }}
        onLoadStart={(syntheticEvent) => {
          setLoading(true);
        }}
        onLoadEnd={(syntheticEvent) => {
          setLoading(false);
        }}
        pullToRefreshEnabled={true}
        injectedJavaScript={runFirst}
        injectedJavaScriptBeforeContentLoaded={runBeforeFirst}
        applicationNameForUserAgent="AtlantidexApp"
        onNavigationStateChange={handleWebViewNavigationStateChange}
        allowsBackForwardNavigationGestures
      />
      {isLoading && (
          <View style={{ flex:1, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(52, 52, 52, 0)' }}>
            <ActivityIndicator size="large" />
          </View>
        )}
    </SafeAreaView>
  );
};