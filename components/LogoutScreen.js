import { SafeAreaView, Text, View, ActivityIndicator, Button } from 'react-native';
import React,{useContext, useState, useRef} from 'react';
import {WebView} from 'react-native-webview';
import { removeAppKeys } from '../tools/Utils';
import {BACKEND} from "@env";
import { SessionContext } from '../contexts/SessionContext';

export default LogoutScreen = ({navigation, route}) => {

    let session = useContext(SessionContext);
    const [isLoading, setLoading] = useState(false);
    const ref = useRef(null);

    function onMessage(data) {

        session.setRole('User');
        session.setApitoken(null);
        session.setUserid(null);
        removeAppKeys().then(() =>{
            alert('Hai effettuato il logout');
        });
      }

    return (
        <SafeAreaView
            style={{
            flex: 1,
            }}
        >
        
        <WebView
            ref={ref}
            originWhitelist={["*"]}
            source={{ uri: BACKEND+'logoutfromapp' }}
            onError={(syntheticEvent) => {
                const { nativeEvent } = syntheticEvent;
                console.warn("WebView error: ", nativeEvent);
            }}
            onLoad={(syntheticEvent) => {
                const { nativeEvent } = syntheticEvent;
                console.warn("WebView loaded: ", nativeEvent);
            }}
            onLoadStart={(syntheticEvent) => {
                setLoading(true);
            }}
            onLoadEnd={(syntheticEvent) => {
                setLoading(false);
            }}
            onMessage={onMessage}
            pullToRefreshEnabled={true}
            applicationNameForUserAgent="AtlantidexApp"
        />
            {isLoading && (
                <View style={{ flex:1, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(52, 52, 52, 0)' }}>
                <ActivityIndicator size="large" />
                </View>
            )}
        </SafeAreaView>
    );
}