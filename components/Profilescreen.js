import { SafeAreaView, Text, View, ActivityIndicator, Button } from 'react-native';
import React,{useContext, useState,useRef} from 'react';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { retriveData, storeData, apiClient } from '../tools/Utils';
import {BACKEND} from "@env";
import { SessionContext } from '../contexts/SessionContext';

export default Profilescreen = ({navigation, route}) => {
  
  let session = useContext(SessionContext);
  const [isLoading, setLoading] = useState(false);
  const ref = useRef(null);

  //const { headerLeftInfo } = route.params;
 /* React.useEffect(() => {
    navigation.setOptions({
      headerLeft:(props) => {
        return (
          <Button 
            title={tit}
            color="#fff"
            onPress={onp}
           />
        )
      }
    });
  }, [route]);*/

  const runFirst = `
      /*setTimeout(function() { */
        const allParas = document.getElementsByTagName("header");
        allParas[0].classList.add("d-none");
       /* }, 100);*/
      true; // note: this is required, or you'll sometimes get silent failures
  `;

  handleWebViewNavigationStateChange = (navState) => {
    console.log('navigation log start');
    console.log(navState);
    console.log('navigation log start');

    if (navState.canGoBack) {
      navigation.setParams({
        headerLeftInfo: {
          title: "indietro",
          onPress: () => ref.current.goBack(),
        },
      });

    } else {
      navigation.setParams({
        headerLeftInfo: null,
      });
    }
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
      }}
    >


      <WebView
        ref={ref}
        originWhitelist={["*"]}
        source={{ uri: BACKEND+'my-atlantinex/personal-data' }}
        onError={(syntheticEvent) => {
          const { nativeEvent } = syntheticEvent;
          console.warn("WebView error: ", nativeEvent);
        }}
        onLoad={(syntheticEvent) => {
          const { nativeEvent } = syntheticEvent;
          //console.warn("WebView loaded: ", nativeEvent);
        }}
        onLoadStart={(syntheticEvent) => {
          setLoading(true);
        }}
        onLoadEnd={(syntheticEvent) => {
          setLoading(false);
        }}
        pullToRefreshEnabled={true}
        applicationNameForUserAgent="AtlantidexApp"
        injectedJavaScript={runFirst}
        onNavigationStateChange={handleWebViewNavigationStateChange}
        allowsBackForwardNavigationGestures
      />
      {isLoading && (
          <View style={{ flex:1, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(52, 52, 52, 0)' }}>
            <ActivityIndicator size="large" />
          </View>
        )}
    </SafeAreaView>
  );
};