import { SafeAreaView, FlatList, Image,  View, ActivityIndicator, Button, TouchableOpacity, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import React,{useState,useRef} from 'react';
import { format } from "date-fns";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { retriveData, storeData, apiClient } from '../tools/Utils';
import {BACKEND} from "@env";






export default BidScreen = ({navigation, route}) => {
  const [isLoading, setLoading] = useState(false);
  const [bids, setBids] = useState();
  React.useEffect(() => {
    navigation.setOptions({
      headerLeft:(props) => {
        return (
          <TouchableWithoutFeedback onPress={() => navigation.toggleDrawer()}>
            <Image
                source={require('../assets/toggle-drawer-icon@3x.ios.png')}
                fadeDuration={0}
            />
          </TouchableWithoutFeedback>
        )
      }
    });
  }, [route]);
  const getBids = async () => {
    retriveData('apitoken').then((value) =>{
      try {
        apiClient.get('api/bid/', 
        { headers: {Authorization: 'Bearer ' + value}})
        .then(r => {
          setBids(r.data);
        }).catch(e => { 
          console.log(e);
        }).finally(()=>{});
      } catch(error) {
          console.log(error);
      };
    });
  }
  React.useEffect(() => {
    getBids();
  }, []);

  const onPress = (row) => {
    navigation.navigate('offerteDet',{ id: row.bid.id});
  }

 

  return (
      <SafeAreaView
          style={{
          flex: 1,
          }}
      >
        <FlatList
            data={bids}
            renderItem={({item, index, separators}) => (
              <TouchableOpacity key={item.id} onPress={() => onPress(item)} style={styles.item}>
                <Image source={{uri:item.product.cover}} style={{width: 60, height: 50, resizeMode: 'contain'}} />
                <Text style={styles.subtitle}>{format(new Date(item.bid.updated_at),'dd/MM/yyyy hh:mm')}</Text>
                <Text style={styles.title}>{item.product.name['it']}</Text>
                <Text style={styles.subtitle}>{item.product.id} {Math.random(10)}</Text>
              </TouchableOpacity>
            )}
            onRefresh={getBids}
            refreshing={false}
        />
      </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  
  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    backgroundColor: '#656446',
  },
  title: {
    fontSize: 32,
  },
});

