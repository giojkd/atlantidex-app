import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {BACKEND} from "@env";



export const storeData = async (key,value) => {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      //console.log(e);
    }
}

export const retriveData = async (key) => {
    try {
        return await AsyncStorage.getItem(key);
    } catch (e) {
        //console.log(e);
    }
}

export const removeAppKeys = async () => {
    let keys = []
    try {
      keys = await AsyncStorage.getAllKeys()
      console.log(`Keys: ${keys}`) // Just to see what's going on
      return await AsyncStorage.multiRemove(keys)
    } catch(e) {
     //console.log(e)
    }
  }

export const apiClient = axios.create({
    baseURL: BACKEND,
    withCredentials: true,
});