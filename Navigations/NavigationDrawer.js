
import React,{useContext, useState, useEffect, useRef } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

//components begin
import PageScreen from '../components/PageScreen';
import LogoutScreen from '../components/LogoutScreen';
import Profilescreen from '../components/Profilescreen';
import NavigationProfile from './NavigationProfile';
//components end
import { SessionContext } from '../contexts/SessionContext';

export default function NavigationDrawer() {
    let session = useContext(SessionContext);
    const Drawer = createDrawerNavigator();
    console.log('------------------------------'+session.role);
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Dashboard" component={PageScreen} initialParams={{page:'admin/dashboard'}}/>
            <Drawer.Screen name="Profilo" component={PageScreen} initialParams={{page:'my-atlantinex/personal-data'}}/>
            <Drawer.Screen name="Dati di Pagamento" component={PageScreen} initialParams={{page:'my-atlantinex/payment-data'}}/>
            <Drawer.Screen name="Offerte" component={NavigationProfile} options={{headerShown:false}} />
            {session.role === 'Admin' &&
            <Drawer.Screen name="Clienti" component={PageScreen} initialParams={{page:'admin/client'}}  />}
            <Drawer.Screen name="Logout" component={LogoutScreen} />
        </Drawer.Navigator>
    );
}