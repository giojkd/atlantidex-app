
import React,{useContext, useState, useEffect, useRef } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import BidScreen from '../components/BidScreen';
import EntityDetail from '../components/EntityDetail';
import { SessionContext } from '../contexts/SessionContext';

export default function NavigationProfile() {
    let session = useContext(SessionContext);
    const ProfileStack = createNativeStackNavigator();
    return (
        <ProfileStack.Navigator>
          <ProfileStack.Screen name="offerte" component={BidScreen} />
          <ProfileStack.Screen name="offerteDet" component={EntityDetail} />
        </ProfileStack.Navigator>
    );
}