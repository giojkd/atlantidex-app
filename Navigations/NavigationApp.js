import { StyleSheet,Platform, Button} from 'react-native';
import React,{useContext, useState, useEffect, useRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

//components begin
import HomeScreen from '../components/Homescreen';
import QrCode from "../components/QrCode";
import NavigationDrawer from './NavigationDrawer';
//components end
import { retriveData } from '../tools/Utils';
import { SessionContext } from '../contexts/SessionContext';
import PageScreen from '../components/PageScreen';

export default function NavigationApp() {
  let session = useContext(SessionContext);
  React.useEffect(() => {
    retriveData('apitoken').then((value) => {
      session.setApitoken(value)
    });
  }, []);
  const Tab = createBottomTabNavigator();
  return (
    <NavigationContainer>
        <Tab.Navigator
          screenOptions={({route}) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
              let iconNames = {
                'home': 'ios-home',
                'search': 'ios-search',
                'profile': 'ios-person',
                'qrcode': 'ios-qr-code',
              };
              iconName = iconNames[route.name];
              return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: '#4e54c8',
            tabBarInactiveTintColor: 'gray',
          })}
        >
          <Tab.Screen 
            name="home" 
            component={HomeScreen} 
            options={{
              title: 'Atlantidex',
              headerTintColor: '#fff',
              headerShadowVisible: false,
              headerStyle: { backgroundColor: '#4e54c8'},
            }} 
          />
          <Tab.Screen name="search" 
            component={HomeScreen} 
            initialParams={{page:'it-it/catalogo'}}
            options={{
              title: 'Cerca',
              headerTintColor: '#fff',
              headerShadowVisible: false,
              headerStyle: { backgroundColor: '#4e54c8'},
            }}
          />
          {session.role !== 'User' &&
          <Tab.Screen name="qrcode" 
            component={HomeScreen}
            initialParams={{page:'admin/my-code'}}
            options={{
              title: 'MyCode',
              headerTintColor: '#fff',
              headerShadowVisible: false,
              headerStyle: { backgroundColor: '#4e54c8'},
            }}
          />}
          {session.role !== 'User' &&
          <Tab.Screen name="profile" 
              component={NavigationDrawer} 
              options={{
                title: 'Profilo',
                headerShown:false,
              }}
          />}
          {session.role === 'User' &&
          <Tab.Screen name="profile" 
              component={HomeScreen} 
              initialParams={{page:'it-it/sign-in-up'}}
              options={{
                title: 'Login',
                headerTintColor: '#fff',
                headerShadowVisible: false,
                headerStyle: { backgroundColor: '#4e54c8'},
              }}
          />}
        </Tab.Navigator>
      </NavigationContainer>
  )
}