import { createContext, useEffect, useState } from "react";

const SessionContext = createContext();

function SessionProvider(props) {
    let basePath = process.env.REACT_APP_BASE_PATH;
    const [userid, setUserid] = useState();
    const [apitoken, setApitoken] = useState();
    const [role, setRole] = useState('User');
    const value = {
        userid: userid,
        setUserid: setUserid,
        apitoken: apitoken,
        setApitoken: setApitoken,
        role: role,
        setRole: setRole,
    };

    
    return (
        <SessionContext.Provider value={value}>
          {props.children}
        </SessionContext.Provider>
    );
}


export { SessionContext, SessionProvider };